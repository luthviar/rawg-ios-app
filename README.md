# RAWG Video Games Database API

## How to run the project?

1. Git clone the project
2. Open file `repogaming.xcodeproj`
3. Run Build via XCode
4. If occur this kind of error: `Missing package product 'RealmSwift'` , please fix the issue by following below instruction:
- `Solution 1: File > Swift Packages > Reset Package Caches` 
- or if the issue still arise, then follow this solution: https://stackoverflow.com/questions/60952549/xcode-11-4-compile-error-missing-package-product-package-name

## Description
This project is Built with SwiftUI, Realm, and ObjectMapper. 
I don't use UIKit because I just read the email technical test at 17 August 2023 , 13:30 WIB and the deadline is 15:00 WIB at 17 August 2023. So, I use SwiftUI with the very short time. 

But do not worry, I also expertise with UIKit too because I am experienced with UIKit more than 3 years, and I just mastered the SwiftUI for about 3 months recently.

You can test me with UIKit too if you want or give me other project with UIKit with proper time.

Thank you.